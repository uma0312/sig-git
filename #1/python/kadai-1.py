# codig: utf-8

from parser import Parser
import glob
import os

# @todo 結果をファイルに出力
if __name__ == '__main__':
    parser = Parser()

    for f in glob.glob('*.rb'):
        # ファイルを開いてパース
        fh = open(f, 'r')
        parsed = parser.parse(fh)
        fh.close()

        for method in parsed.methods:
            if method.args is None:
                print("メソッド名,{0},{1}".format(method.name, "引数無し"))
            else:
                print("メソッド名,{0},{1}".format(method.name, ",".join(method.args)))

        print("行数,{0}".format(parsed.line_length))
        print("文字数,{0:.2f},{1:.2f}".format(parsed.mean_length(), parsed.variance_length()))
        print("メソッド数,{0}".format(len(parsed.methods)))
        print("制御文", end="")
        for num in parsed.num_of_reserved_words.values():
            print(",{0}".format(num), end="")
        print()

    print("平均メソッド数, {0:.2f}".format(parser.mean_methods()))
