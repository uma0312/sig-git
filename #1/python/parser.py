# coding: utf-8

import re
from collections import defaultdict
from method import Method
from method import MethodList
import numpy as np

class Parser:

    def __init__(self):
        self.reserved_words = ["if", "while", "until", "each", "for", "loop"]
        self.pat_method = re.compile(r"def\s+(?P<method_name>[a-zA-Z_][\w\?!]*)\s*\(?(?P<args>[\w\s,]*)?\)?")
        self.pat_reserved_word = re.compile(r"\b%s\b" % "|".join(self.reserved_words))
        self.methods = []

    def parse(self, fh):
        parsed = Parsed(fh)
        for line in fh:
            parsed.line_length += 1 # 行数を1増やす

            # コメント行の消去
            line = re.sub(r"#.*", "", line).strip()

            # 文字数をカウント
            #np.append(parsed.str_length, len(line))
            parsed.str_length.append(len(line))

            # メソッド行
            method = self.pat_method.search(line)
            if method is not None:
                method_name = str(method.group("method_name"))
                if len(method.group("args")) > 0:
                    args = re.sub(r'\s+', "", str(method.group("args"))).split(",")
                    parsed.methods.append(Method(method_name, args))
                else:
                    parsed.methods.append(Method(method_name, None))

            reserved_word = self.pat_reserved_word.search(line)
            if reserved_word is not None:
                reserved_word = str(reserved_word.group(0))
                parsed.num_of_reserved_words[reserved_word] += 1

        # メソッド数
        self.methods.append(len(parsed.methods))

        return parsed

    def mean_methods(self):
        return np.mean(np.array(self.methods))

class Parsed:
    def __init__(self, fh):
        self.methods = MethodList()
        self.line_length = 0 # 行数
        #self.str_length = np.array([])  # 文字数
        self.str_length = []  # 文字数

        # @todo defaultdictだと取り出す順番が
        #   保持されないからOrderedDictに変える
        self.num_of_reserved_words = defaultdict(lambda: 0)

    def mean_length(self):
        return np.mean(np.array(self.str_length))

    def variance_length(self):
        return np.var(np.array(self.str_length))
