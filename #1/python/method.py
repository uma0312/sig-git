# coding: utf-8

class Method:
    def __init__(self, method_name, args):
        self.name = method_name
        self.args = args

class MethodList:
    def __init__(self):
        self.methods = []

    def __iter__(self):
        for method in self.methods:
            yield method

    def append(self, method):
        self.methods.append(method)

    def __len__(self):
        return len(self.methods)
