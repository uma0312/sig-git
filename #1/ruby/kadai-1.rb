# coding: utf-8

class Array
  # Calculate mean
  # @return [float] mean
  def mean
    self.inject(:+) / self.size.to_f
  end

  # Calculate variance
  # @return [float] variance
  def variance
    self.inject(0){|sum, num| sum + (num - self.mean) ** 2} / self.size
  end
end

# 条件構造語のリスト
reserved_words = %w(
  if
  while
  until
  each
  for
  loop
)

num_of_methods = [] # それぞれのファイルのメソッド数
num_of_lines = []   # それぞれのファイルの行数

# メソッド定義の行にマッチするパターン
pattern_of_method_definition = /^def\s+(?<method_name>[a-zA-Z_][\w\?!]*)\s*\(?(?<args>[\w\s,]*)?\)?/
# 条件構造にマッチするパターン
pattern_of_reserved_word = /\b(#{reserved_words.join("|")})\b/

# 条件構造語とそれぞれのファイルでの出現回数
# {if => [0, 1, 1, ...], while => [1, 0, 1, ...], ...}
num_of_reserved_words = {}
reserved_words.each do |element|
  num_of_reserved_words[element] = []
end

Dir.glob("*.rb") do |filename|
  next if filename == __FILE__ # このファイル自身は処理しない

  # 条件構造語とその出現回数
  reserved_list = {}
  reserved_words.each do |element|
    reserved_list[element] = 0
  end

  # メソッド名とその引数のリスト
  # {method1 => [aaa, bbb, ccc], method2 => [xxx, yyy], ...}
  methods = Hash.new

  num_of_line = 0          # ファイルの行数
  line_lengths = Array.new # 各行の文字数

  # ファイルを読み込んで処理
  File.open(filename, "r") do |file|
    while line = file.gets
      line.sub!(/#.*/, "") # コメントの除去(コメントだけの行は空行に)
      line.strip!

      case line
      # メソッド定義の行
      when pattern_of_method_definition
        method_name = $~[:method_name]
        args = $~[:args].gsub(/\s+/, "").split(",")
        methods[method_name] = args
      # 空行
      when ""
        next
      # 条件構造語の行
      # 1行に複数個出現しないと考える
      when pattern_of_reserved_word
        reserved_list[$1] += 1
      end

      num_of_line += 1
      line_lengths << line.size
    end
  end

  num_of_methods << methods.size
  num_of_lines << num_of_line

  # 入力ファイルの集計結果を出力
  File.open("value-#{File.basename(filename, '.rb')}.csv", "w") do |file|
    # メソッドとその引数の表示
    methods.keys.each do |key|
      if methods[key].size == 0
        file.puts "メソッド,#{key},引数無し"
      else
        file.puts "メソッド,#{key},#{methods[key].join(',')}"
      end
    end

    file.puts "行数,#{num_of_line}"
    file.puts "文字数,%.2f,%.2f" % [line_lengths.mean.round(2), line_lengths.variance.round(2)]
    file.puts "メソッド数,#{methods.size}"
    file.print "制御文,#{reserved_list["if"]}," # if文の数
    file.print reserved_list.values[1..-1].inject(:+), "," # 繰り返し文の合計数
    file.puts reserved_list.values[1..-1].join(",") # 繰り返し文の数
  end

  # 制御文の数をnum_of_reserved_wordsに保存
  reserved_list.each do |key, value|
    num_of_reserved_words[key] << value
  end

end

# summaryの書き出し
File.open("summary.csv", "w") do |file|
  file.puts "平均メソッド数, %.2f" % num_of_methods.mean.round(2)
  file.puts "平均行数, %.2f" % num_of_lines.mean.round(2)
  num_of_reserved_words.each do |key, value|
    file.puts "%sの平均出現回数, %.2f" % [key, value.mean.round(2)]
  end
end
